import * as React from "react";
import * as ReactDOM from "react-dom";
import { HotApp } from "./App";

ReactDOM.render(<HotApp />, document.getElementById("root"));
