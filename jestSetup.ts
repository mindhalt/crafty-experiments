declare namespace NodeJS {
  interface Global {
    $: JQuery;
    jQuery: JQuery;
  }
}

global.$ = global.jQuery = require("jquery");

document.documentElement.innerHTML = `
  <div id="root"></div>
`;
