import * as React from "react";
import { hot } from "react-hot-loader";
import "../css/app.scss";

const App = () => <h1>Hello World!</h1>;

const HotApp = hot(module)(App);

export { App, HotApp };
