import * as React from "react";
import { App } from "../App";
import { shallow } from "enzyme";

describe("App", () => {
  beforeAll(() => {
    document.documentElement.innerHTML = `
      <div id="root">
        <h1>Crafty experiments</h1>
        <p>Just random page to test theories.</p>
      </div>
    `;
  });

  test("should return name", () => {
    const app = <App />;
    const $app = $("#root");

    expect(app).toMatchSnapshot();
    expect(shallow(<App />)).toMatchSnapshot();
    expect($app.length).toBeGreaterThan(0);
  });
});
