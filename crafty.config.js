module.exports = {
  presets: [
    "@swissquote/crafty-preset-babel",
    "@swissquote/crafty-preset-postcss",
    "@swissquote/crafty-preset-jest",
    "@swissquote/crafty-preset-typescript",
    "@swissquote/crafty-runner-webpack",
    "@swissquote/crafty-preset-react",
    "@swissquote/crafty-runner-gulp"
  ],
  js: {
    app: {
      runner: "webpack",
      source: "src/js/index.tsx",
      hot: true,
      react: true
    }
  },
  css: {
    app: {
      source: "src/css/app.scss",
      watch: ["css/**"]
    }
  },
  jest(crafty, options) {
    options.transform["^.+\\.tsx?$"] = require.resolve("ts-jest");
    options.setupFiles = ["<rootDir>/jestSetup.ts"];
    // options.globals = {
    //   "ts-jest": {
    //     "diagnostics": true
    //   }
    // };
  }
};
